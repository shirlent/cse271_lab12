/**
 * Lab 12 class that uses recursion to compute two
 * difference values. The powerN method finds an integer
 * to the nth degree, while the triangle method finds the 
 * number of blocks in a 'n' tall triangle.
 * 
 * @author nickshirley
 */

public class Recursion {
    public static void main(String[] args) {
        // testing statements
        System.out.println(powerN(3,1));
        System.out.println(powerN(3,2));
        System.out.println(powerN(3,3));
        
        System.out.println(triangle(0));
        System.out.println(triangle(1));
        System.out.println(triangle(2));
        System.out.println(triangle(3));
    }
    
    /**
     * Method that computes a value to the nth power
     * using recursion.
     * 
     * @param base Number being added to itself
     * @param n How many times to add the value 
     * @return The value of the base to the nth degree
     */
    public static int powerN(int base, int n) {
        // base case
        if (n == 0) { 
            return 0;
        }
        // adds the base until n is equal to 0
        return base + powerN(base, n - 1);
    }
    
    /**
     * Method that computes the amount of blocks within a 
     * triangle.
     * 
     * @param row Number of rows in the triangle
     * @return The number of blocks within the triangle
     */
    public static int triangle(int row) {
        // base case
        if (row == 0) {
            return 0;
        }
        
        // adds the row until it is equal to 0
        return row + triangle(row - 1);
        
    }
 
}
